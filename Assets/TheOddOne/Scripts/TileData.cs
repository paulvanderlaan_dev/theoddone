﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TileData", menuName = "ScriptableObjects/TileData", order = 1)]
public class TileData : ScriptableObject
{
    public string tileName;
    public Sprite sprite;
    public AudioClip audioClip;
    public float audioClipVolume = 1f;
}
