﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using UnityEngine.UI;


namespace TheOddOne
{
    public class GameController : MonoBehaviourPunCallbacks
    {
        public int maxScore = 5;
        public float showOddTileDuration = 5f;

        public float wrongTileInactivityDuration = 1f;

        [SerializeField] private TileGenerator tileGenerator;
        [SerializeField] private GameEndPanel gameEndPanel;
        [SerializeField] private GameStartPanel gameStartPanel;
        [SerializeField] private GameObject gameExitPanel;
        [SerializeField] private Button exitButton;
        [SerializeField] private Timer timer;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip tileCorrectClip;
        [SerializeField] private AudioClip gameWonClip;
        [SerializeField] private AudioClip gameLostClip;
        [SerializeField] private Text InfoText;
        [SerializeField] private CountdownTimer countdownTimer;

        [SerializeField] string menuSceneToLoad = "TheOddOne-LobbyScene";

        private bool gameEnded = false;

        private float showRestartDelay = 3f;

        #region UNITY

        public override void OnEnable()
        {
            base.OnEnable();
            tileGenerator.OnCorrectTileChosen += HandleCorrectTileChosen;
            tileGenerator.OnWrongTileChosen += HandleWrongTileChosen;
            CountdownTimer.OnCountdownTimerHasExpired += OnCountdownTimerIsExpired;
        }

        public override void OnDisable()
        {
            base.OnDisable();
            tileGenerator.OnCorrectTileChosen -= HandleCorrectTileChosen;
            tileGenerator.OnWrongTileChosen -= HandleWrongTileChosen;
            CountdownTimer.OnCountdownTimerHasExpired -= OnCountdownTimerIsExpired;
        }

        public void Start()
        {
            Hashtable props = new Hashtable
            {
                {TheOddOneGame.PLAYER_LOADED_LEVEL, true}
            };
            PhotonNetwork.LocalPlayer.SetCustomProperties(props);
            Initialize();
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Escape))  //Back button on Android
            {
                HandleVirtualBackButtonPressed();
            }
        }

        #endregion

        void Initialize()
        {
            object maxScoreValue;
            PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(TheOddOneGame.MAX_SCORE, out maxScoreValue);
            print("max score in room: " + (int)maxScoreValue);
            maxScore = (int)maxScoreValue;

            object difficulty;
            PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(TheOddOneGame.DIFFICULTY, out difficulty);
            print("difficulty in room: " + (string)difficulty);
            SetDifficulty((string)difficulty);
        }


        #region UI CALLBACKS

        public void HandleMenuButtonPressed()
        {
            PhotonNetwork.LeaveRoom();
        }

        public void HandleVirtualBackButtonPressed()
        {
            if(gameExitPanel.activeInHierarchy)
            {
                gameExitPanel.SetActive(false);
            }
            else if(!gameEnded)
            {
                gameExitPanel.SetActive(true);
            }
        }

        public void HandleReplayButtonPressed()
        {
            photonView.RPC(nameof(ReplayGame), RpcTarget.All);
            gameEndPanel.replayButton.interactable = false;
        }

        #endregion

        #region PUN CALLBACKS

        public override void OnLeftRoom()
        {
            print("left room");
            PhotonNetwork.Disconnect();
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            print("disconnected: " + cause.ToString());

            SceneManager.LoadScene(menuSceneToLoad);
        }

        [PunRPC]
        void NextRound(PhotonMessageInfo info)
        {
            StartCoroutine(NextRoundRoutine(info.Sender));
        }

        [PunRPC]
        public void ReplayGame()
        {
            StartCoroutine(ReplayGameRoutine());
        }

        [PunRPC]
        public void ShowOpponentChoseWrongTile(int tileId, PhotonMessageInfo info)
        {
            tileGenerator.ShowOpponentPressedWrongTile(tileId, info.Sender);
        }

        public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
        {
            foreach(var player in PhotonNetwork.PlayerList)
            {
                if(player.GetScore() >= maxScore && !gameEnded)
                {
                    gameEnded = true;
                    gameEndPanel.Show(player);
                    exitButton.gameObject.SetActive(false);
                    audioSource.Stop();
                    print(player.NickName + " has won!");
                }
            }

            print(targetPlayer.NickName + " has new score: " + targetPlayer.GetScore());

            if(!PhotonNetwork.IsMasterClient)
            {
                return;
            }

            // if there was no countdown yet, the master client (this one) waits until everyone loaded the level and sets a timer start
            int startTimestamp;
            bool startTimeIsSet = CountdownTimer.TryGetStartTime(out startTimestamp);

            if(changedProps.ContainsKey(TheOddOneGame.PLAYER_LOADED_LEVEL))
            {
                if(CheckAllPlayerLoadedLevel())
                {
                    if(!startTimeIsSet)
                    {
                        CountdownTimer.SetStartTime();
                    }
                }
                else
                {
                    // not all players loaded yet. wait:
                    Debug.Log("setting text waiting for players! ", this.InfoText);
                    InfoText.text = "Waiting for other players...";
                }
            }
        }

        #endregion

        private void OnCountdownTimerIsExpired()
        {
            StartGame();
        }

        void StartGame()
        {
            if(PhotonNetwork.IsMasterClient)
            {
                tileGenerator.CreateAndSetRoomTileProperties();
            }

            gameStartPanel.gameObject.SetActive(false);
            exitButton.gameObject.SetActive(true);
            audioSource.Play();
        }

        void SetDifficulty(string difficulty)  //TODO remove string based method and expose settings data in Scriptable Object
        {
            switch(difficulty)
            {
                case TheOddOneGame.EASY:
                    tileGenerator.minimumSets = 3;
                    tileGenerator.maximumSets = 4;
                    break;
                case TheOddOneGame.NORMAL:
                    tileGenerator.minimumSets = 5;
                    tileGenerator.maximumSets = 8;
                    break;
                case TheOddOneGame.HARD:
                    tileGenerator.minimumSets = 8;
                    tileGenerator.maximumSets = 12;
                    break;
                default:
                    tileGenerator.minimumSets = 4;
                    tileGenerator.maximumSets = 4;
                    break;
            }
        }

        IEnumerator NextRoundRoutine(Player player)
        {

            yield return tileGenerator.StartCoroutine(tileGenerator.ShowOddTile(showOddTileDuration, player));

            if(PhotonNetwork.IsMasterClient && !gameEnded)
            {
                tileGenerator.CreateAndSetRoomTileProperties();
            }

            yield return null;
        }

        void HandleCorrectTileChosen(Tile tile)
        {
            PhotonNetwork.LocalPlayer.AddScore(1);
            photonView.RPC(nameof(NextRound), RpcTarget.AllViaServer);
            print(PhotonNetwork.LocalPlayer.ActorNumber + " - " + TheOddOneGame.GetColor(PhotonNetwork.LocalPlayer.GetPlayerNumber()));
            AudioSource.PlayClipAtPoint(tileCorrectClip, Camera.main.transform.position);
            tile.ShowSuccess();
        }

        void HandleWrongTileChosen(Tile tile)
        {
            PhotonNetwork.LocalPlayer.AddScore(-1);
            AudioSource.PlayClipAtPoint(gameLostClip, Camera.main.transform.position);
            tile.ShowFail();
            tileGenerator.StartCoroutine(tileGenerator.MakeTilesNonInteractible(wrongTileInactivityDuration));
            photonView.RPC(nameof(ShowOpponentChoseWrongTile), RpcTarget.Others, tile.id);
        }

        IEnumerator ReplayGameRoutine()
        {
            gameEndPanel.ShowRestarting();
            PhotonNetwork.LocalPlayer.SetScore(0);
            yield return new WaitForSeconds(showRestartDelay);

            gameEndPanel.gameObject.SetActive(false);
            gameStartPanel.gameObject.SetActive(true);
            tileGenerator.RemoveAllTiles();

            gameEnded = false;

            if(PhotonNetwork.IsMasterClient)
            {
                CountdownTimer.SetStartTime();
            }

            yield return new WaitForSeconds(1f); //delay so start time should be set before timer initialize
            countdownTimer.enabled = true;
        }

        public void ResetPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
            print("player prefs reset");
        }


        private bool CheckAllPlayerLoadedLevel()
        {
            foreach(Player p in PhotonNetwork.PlayerList)
            {
                object playerLoadedLevel;

                if(p.CustomProperties.TryGetValue(TheOddOneGame.PLAYER_LOADED_LEVEL, out playerLoadedLevel))
                {
                    if((bool)playerLoadedLevel)
                    {
                        continue;
                    }
                }

                return false;
            }

            return true;
        }



    }
}
