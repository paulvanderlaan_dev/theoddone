﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;

namespace TheOddOne
{
    public class PlayerResultsEntry : MonoBehaviour
    {
        [SerializeField] private TMP_Text placementText;
        [SerializeField] private TMP_Text playerNameText;
        [SerializeField] private TMP_Text playerScoreText;

        public void Initialize(Player player, int placement)
        {
            placementText.text = GetPlacementString(placement);
            playerNameText.color = TheOddOneGame.GetColor(player.GetPlayerNumber());
            playerNameText.text = player.NickName;
            playerScoreText.text = player.GetScore().ToString();
        }

        string GetPlacementString(int place)
        {
            switch(place)
            {
                case 1:
                    return "1st";
                case 2:
                    return "2nd";
                case 3:
                    return "3rd";
                default:
                    return place + "th";

            }
        }
    }
}
