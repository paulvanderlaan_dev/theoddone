﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TheOddOne
{
    public static class Utilities
    {
        /// <summary>
        /// Shuffles the element order of the specified list.
        /// </summary>
        public static void Shuffle<T>(this IList<T> currentList)
        {
            var count = currentList.Count;
            var last = count - 1;
            for(var i = 0; i < last; ++i)
            {
                var randomNumber = UnityEngine.Random.Range(i, count);
                var temporaryObject = currentList[i];
                currentList[i] = currentList[randomNumber];
                currentList[randomNumber] = temporaryObject;
            }
        }
    }
}
