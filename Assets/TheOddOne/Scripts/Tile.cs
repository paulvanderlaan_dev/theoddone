﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using DG.Tweening;
using Photon.Realtime;
using TMPro;
using Photon.Pun.UtilityScripts;


namespace TheOddOne
{
    public class Tile : MonoBehaviour
    {
        public TileData tileData;

        [SerializeField] private SpriteRenderer tileSpriteRenderer;
        [SerializeField] private SpriteRenderer ColorRingSpriteRenderer;
        [SerializeField] private GameObject hoverFXObject;
        [SerializeField] private SpriteRenderer wrongSpriteRenderer;
        [SerializeField] private GameObject SuccessFXObject;
        [SerializeField] private AudioClip hoverSound;
        [SerializeField] private DOTweenAnimation wrongAnimation;
        [SerializeField] private DOTweenAnimation correctAnimation;
        [SerializeField] private TMP_Text playerNameText;

        public bool isOdd;
        public bool interactible = true;
        public bool isWronglyPressed { get; private set; }

        public SpriteRenderer GetSpriteRenderer { get { return tileSpriteRenderer; } }

        public int id { get; private set; }

        public event Action<Tile> OnTilePressed;

        public void Initialize(TileData tileData, bool isOdd, int id)
        {
            this.tileData = tileData;
            this.isOdd = isOdd;
            name = tileData.name;
            tileSpriteRenderer.sprite = tileData.sprite;
            this.id = id;

        }

        public void ShowPlayerData(Color color, string text)
        {
            ColorRingSpriteRenderer.gameObject.SetActive(true);
            ColorRingSpriteRenderer.color = color;
            playerNameText.color = color;
            playerNameText.text = text;
            playerNameText.gameObject.SetActive(true);
            tileSpriteRenderer.sortingOrder = 4;
        }

        public void ShowSuccess()
        {
            SuccessFXObject.SetActive(true);
            correctAnimation.DOPlayById("Correct");
            tileSpriteRenderer.sortingOrder = 4;
        }

        public void ShowFail()
        {
            wrongSpriteRenderer.gameObject.SetActive(true);
            interactible = false;
            isWronglyPressed = true;
            wrongAnimation.DOPlayById("Wrong");
        }

        public IEnumerator ShowOpponentFail(Player player)
        {
            print("within coroutine");
            wrongAnimation.DOPlayById("Wrong");
            playerNameText.color = TheOddOneGame.GetColor(player.GetPlayerNumber());
            playerNameText.text = player.NickName;
            playerNameText.gameObject.SetActive(true);
            yield return new WaitForSeconds(1f);
            playerNameText.gameObject.SetActive(false);
        }

        private void OnMouseDown()
        {
            if(!interactible || EventSystem.current.IsPointerOverGameObject())
                return;


            OnTilePressed?.Invoke(this);
        }

#if !UNITY_ANDROID || UNITY_EDITOR
        private void OnMouseEnter()
        {


            if(!interactible || EventSystem.current.IsPointerOverGameObject())
                return;

            if(hoverSound != null)
            {
                AudioSource.PlayClipAtPoint(hoverSound, Camera.main.transform.position);
            }
            hoverFXObject.SetActive(true);
        }

        private void OnMouseExit()
        {
            hoverFXObject.SetActive(false);
        }

#endif

    }
}
