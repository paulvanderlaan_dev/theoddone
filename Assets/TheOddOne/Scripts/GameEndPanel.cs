﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Pun.UtilityScripts;
using System.Linq;


namespace TheOddOne
{
    public class GameEndPanel : MonoBehaviour
    {
        [SerializeField] private TMP_Text restartingText;
        [SerializeField] private AudioClip highScoreSound;
        [SerializeField] private Transform playerResultsParent;
        [SerializeField] private PlayerResultsEntry playerResultsEntryPrefab;
        public Button replayButton;

        private List<PlayerResultsEntry> currentEntries = new List<PlayerResultsEntry>();

        private void OnEnable()
        {
            replayButton.interactable = PhotonNetwork.IsMasterClient;
            restartingText.gameObject.SetActive(false);
        }

        public void Show(Player winner)
        {
            gameObject.SetActive(true);
            ShowPlayerResults();
        }

        void ShowPlayerResults()
        {
            ClearPlayerResultsEntries();

            var orderedPlayerList = Photon.Pun.PhotonNetwork.PlayerList.OrderByDescending(player => player.GetScore());

            int placement = 1;
            foreach(var player in orderedPlayerList)
            {
                var playerResultsEntry = Instantiate(playerResultsEntryPrefab, playerResultsParent);
                currentEntries.Add(playerResultsEntry);
                playerResultsEntry.Initialize(player, placement);
                placement++;
            }
        }

        void ClearPlayerResultsEntries()
        {
            foreach(var entry in currentEntries)
            {
                Destroy(entry.gameObject);
            }
            currentEntries.Clear();
        }

        public void ShowRestarting()
        {
            restartingText.gameObject.SetActive(true);
        }
    }
}
