﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TheOddOne
{
    public class TheOddOneGame
    {
        public const string PLAYER_READY = "IsPlayerReady";
        public const string PLAYER_LOADED_LEVEL = "PlayerLoadedLevel";

        public const string MAX_SCORE = "MaxScore";
        public const string DIFFICULTY = "Difficulty";

        public const string EASY = "Easy";
        public const string NORMAL = "Normal";
        public const string HARD = "Hard";

        public static Color GetColor(int colorChoice)  //TODO put player colors in Scriptable Object
        {
            Color color = Color.black;
            
            switch(colorChoice)
            {
                case 0:
                    ColorUtility.TryParseHtmlString("#D1241F", out color);
                    break;
                case 1:
                    ColorUtility.TryParseHtmlString("#006A0B", out color);
                    break;
                case 2:
                    ColorUtility.TryParseHtmlString("#3544C5", out color);
                    break;
                case 3:
                    ColorUtility.TryParseHtmlString("#E28C1B", out color);
                    break;
                case 4:
                    ColorUtility.TryParseHtmlString("#2DAFBC", out color);
                    break;
                case 5:
                    ColorUtility.TryParseHtmlString("#BA40E5", out color);
                    break;
                case 6:
                    ColorUtility.TryParseHtmlString("#9C591B", out color);
                    break;
                case 7:
                    ColorUtility.TryParseHtmlString("#FF58AF", out color);
                    break;
            }

            return color;
        }

        //public static Color GetColor(int colorChoice)
        //{
        //    switch(colorChoice)
        //    {
        //        case 0:
        //            return Color.red;
        //        case 1:
        //            return Color.green;
        //        case 2:
        //            return Color.blue;
        //        case 3:
        //            return Color.yellow;
        //        case 4:
        //            return Color.cyan;
        //        case 5:
        //            return Color.grey;
        //        case 6:
        //            return Color.magenta;
        //        case 7:
        //            return Color.white;
        //    }

        //    return Color.black;
        //}
    }
}
