﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class ConnectionStatusText : MonoBehaviour
{
    [SerializeField] private string connectionStatusMessage = "    Connection Status: ";

    [Header("UI References")]
    public TMP_Text statusText;

    #region UNITY

    public void Update()
    {
        statusText.text = connectionStatusMessage + PhotonNetwork.NetworkClientState;
    }

    #endregion
}
