﻿using System.Collections;
using System.Collections.Generic;
using TheOddOne;
using UnityEngine;

public class GamePlayTester : MonoBehaviour
{


    [SerializeField] private KeyCode respawnTilesKey = KeyCode.T;
    [SerializeField] private TileGenerator tileGenerator;

    [SerializeField] private TileData[] tiles;

#if UNITY_EDITOR
    private void Update()
    {
        if(Input.GetKeyDown(respawnTilesKey))
        {
            tileGenerator.CreateAndSetRoomTileProperties();
        }

        if(Input.GetKeyDown(KeyCode.Alpha0))
        {
            AudioSource.PlayClipAtPoint(tiles[0].audioClip, Camera.main.transform.position, tiles[0].audioClipVolume);
        }

        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            AudioSource.PlayClipAtPoint(tiles[1].audioClip, Camera.main.transform.position, tiles[1].audioClipVolume);
        }

        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            AudioSource.PlayClipAtPoint(tiles[2].audioClip, Camera.main.transform.position, tiles[2].audioClipVolume);
        }

        if(Input.GetKeyDown(KeyCode.Alpha3))
        {
            AudioSource.PlayClipAtPoint(tiles[3].audioClip, Camera.main.transform.position, tiles[3].audioClipVolume);
        }

        if(Input.GetKeyDown(KeyCode.Alpha4))
        {
            AudioSource.PlayClipAtPoint(tiles[4].audioClip, Camera.main.transform.position, tiles[4].audioClipVolume);
        }

        if(Input.GetKeyDown(KeyCode.Alpha5))
        {
            AudioSource.PlayClipAtPoint(tiles[5].audioClip, Camera.main.transform.position, tiles[5].audioClipVolume);
        }

        if(Input.GetKeyDown(KeyCode.Alpha6))
        {
            AudioSource.PlayClipAtPoint(tiles[6].audioClip, Camera.main.transform.position, tiles[6].audioClipVolume);
        }

        if(Input.GetKeyDown(KeyCode.Alpha7))
        {
            AudioSource.PlayClipAtPoint(tiles[7].audioClip, Camera.main.transform.position, tiles[7].audioClipVolume);
        }

        if(Input.GetKeyDown(KeyCode.Alpha8))
        {
            AudioSource.PlayClipAtPoint(tiles[8].audioClip, Camera.main.transform.position, tiles[8].audioClipVolume);
        }

        if(Input.GetKeyDown(KeyCode.Alpha9))
        {
            AudioSource.PlayClipAtPoint(tiles[9].audioClip, Camera.main.transform.position, tiles[9].audioClipVolume);
        }

    }
#endif
}


