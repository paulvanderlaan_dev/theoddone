﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;

namespace TheOddOne
{
    public class TileInstanceData
    {
        public int tileId;
        public Vector2 tilePosition;
        public bool isOdd;

        public TileInstanceData(int tileId, Vector2 tilePosition, bool isOdd)
        {
            this.tileId = tileId;
            this.tilePosition = tilePosition;
            this.isOdd = isOdd;
        }

    }

    public class TileGenerator : MonoBehaviourPunCallbacks
    {
        [SerializeField] private TileData[] tiles;
        [SerializeField] private Tile tilePrefab;

        //a set is a pair/trio/quartet etc of duplicate tiles
        public int setSize = 2;
        public int minimumSets = 2;
        public int maximumSets = 4;

        public float spawnAreaWidth = 10f;
        public float spawnAreaHeight = 10f;

        public float spawnDelay = 0.1f;  //delay between tile spawns

        public event System.Action<Tile> OnCorrectTileChosen;
        public event System.Action<Tile> OnWrongTileChosen;

        public List<Tile> spawnedTiles;

        [SerializeField] private AudioClip spawnSound;

        private float overlapCheckRadius = 0.645f;  // should be same size as tile radius
        private int overlapMaxChecks = 10000;  //max amount of picking random locations when overlap check fails

        public readonly string TileIdPropertiesKey = "TileIds";
        public readonly string TilePositionPropertiesKey = "TilePositions";
        public readonly string TileIsOddPropertiesKey = "TileIsOddValues";

        private bool roundEnded = false;

        TileInstanceData[] GenerateTilesData()
        {
            List<TileInstanceData> tileInstancesData = new List<TileInstanceData>();

            int setsToSpawnCount = Random.Range(minimumSets, maximumSets);
            var tileList = tiles.ToList();
            var tilePositions = GetRandomCirclePositionsNoOverlap(spawnAreaWidth,spawnAreaHeight, overlapCheckRadius, setsToSpawnCount * setSize + 1);

            var oddTileId = Random.Range(0, tileList.Count);
            tileList.Remove(tileList[oddTileId]);

            tileInstancesData.Add(new TileInstanceData(oddTileId, tilePositions[0], true));


            for(int i = 0; i < setsToSpawnCount; i++)
            {
                var chosenTileId = Random.Range(0,tileList.Count);
                var chosenTile = tileList[chosenTileId];
                tileList.Remove(chosenTile);

                var tileIndexOriginalArray = System.Array.IndexOf(tiles, chosenTile);

                for(int j = 0; j < setSize; j++)
                {

                    tileInstancesData.Add(new TileInstanceData(tileIndexOriginalArray, tilePositions[i * setSize + j + 1], false));
                }
            }
            tileInstancesData.Shuffle();
            return tileInstancesData.ToArray();
        }

        public void CreateAndSetRoomTileProperties()
        {
            var tileInstancesData = GenerateTilesData();

            //creating separate arrays so data can be send over network
            int[] tileIds = new int[tileInstancesData.Length];
            Vector2[] tilePositions = new Vector2[tileInstancesData.Length];
            bool[] isOddData = new bool[tileInstancesData.Length];

            for(int i = 0; i < tileInstancesData.Length; i++)
            {
                tileIds[i] = tileInstancesData[i].tileId;
                tilePositions[i] = tileInstancesData[i].tilePosition;
                isOddData[i] = tileInstancesData[i].isOdd;
            }

            Hashtable properties = new Hashtable
        {
            {TileIdPropertiesKey, tileIds },
            {TilePositionPropertiesKey, tilePositions },
            { TileIsOddPropertiesKey, isOddData }
        };

            PhotonNetwork.CurrentRoom.SetCustomProperties(properties);  //send tiles data to server, processed by OnRoomPropertiesUpdate()
        }



        public IEnumerator ShowOddTile(float duration, Player player)
        {
            roundEnded = true;

            Tile oddTile = null;

            var playerColor = TheOddOneGame.GetColor(player.GetPlayerNumber());

            foreach(var tile in spawnedTiles)
            {
                tile.interactible = false;

                if(!tile.isOdd)
                {
                    tile.GetSpriteRenderer.color = new Color(1f, 1f, 1f, 0.5f);
                }
                else
                {
                    oddTile = tile;
                    oddTile.ShowPlayerData(playerColor, player.NickName);
                }
            }

            if(oddTile != null)
            {
                AudioSource.PlayClipAtPoint(oddTile.tileData.audioClip, Camera.main.transform.position, oddTile.tileData.audioClipVolume);
            }

            yield return new WaitForSeconds(duration);
        }

        public IEnumerator MakeTilesNonInteractible(float duration)
        {
            foreach(var tile in spawnedTiles)
            {
                tile.interactible = false;
                tile.GetSpriteRenderer.color = new Color(1f, 1f, 1f, 0.5f);
            }
            yield return new WaitForSeconds(duration);

            if(!roundEnded)
            {
                foreach(var tile in spawnedTiles)
                {
                    if(!tile.isWronglyPressed)
                    {
                        tile.interactible = true;
                    }

                    tile.GetSpriteRenderer.color = new Color(1f, 1f, 1f, 1f);
                }
            }
        }


        public void RemoveAllTiles()
        {
            StopAllCoroutines();
            foreach(var tile in spawnedTiles)
            {
                Destroy(tile.gameObject);
            }
            spawnedTiles.Clear();
        }

        public void GenerateNewTile(TileData tileData, Vector2 spawnPosition, bool isOdd, int id)
        {
            Tile newTile = Instantiate(tilePrefab, spawnPosition, Quaternion.identity, transform);
            newTile.Initialize(tileData, isOdd, id);
            newTile.OnTilePressed += HandleTilePressed;
            spawnedTiles.Add(newTile);
            AudioSource.PlayClipAtPoint(spawnSound, Vector3.zero);
        }

        void HandleTilePressed(Tile pressedTile)
        {
            if(pressedTile.isOdd)
            {
                OnCorrectTileChosen?.Invoke(pressedTile);
            }
            else
            {
                OnWrongTileChosen?.Invoke(pressedTile);
            }
        }

        public void ShowOpponentPressedWrongTile(int tileId, Player player)
        {
            var wrongTile = spawnedTiles[tileId];
            wrongTile.StartCoroutine(wrongTile.ShowOpponentFail(player));
        }


        //Creates array of nonoverlapping circle positions by brute force
        Vector2[] GetRandomCirclePositionsNoOverlap(float width, float height, float circleRadius, int positionsAmount)
        {
            List<Vector2> positions = new List<Vector2>();

            int tries = 0;


            positions.Add(GetRandomPosition(spawnAreaWidth, spawnAreaHeight)); //add initial point

            for(int i = 0; i < positionsAmount && tries < overlapMaxChecks;)
            {
                Vector2 randomPoint = GetRandomPosition(spawnAreaWidth,spawnAreaHeight);

                float minimumDistance = float.MaxValue;
                for(int j = 0; j < positions.Count; j++)
                {
                    float currentDistance = Vector2.Distance(randomPoint, positions[j]);

                    if(currentDistance < minimumDistance)
                    {
                        minimumDistance = currentDistance;
                    }


                }
                if(minimumDistance > circleRadius * 2f) //found suitable point
                {
                    positions.Add(randomPoint);
                    i++;
                    tries = 0;
                }
                else
                {
                    tries++;
                }
            }


            return positions.ToArray();
        }

        Vector2 GetRandomPosition(float areaWidth, float areaHeight)
        {
            float x = Random.Range(areaWidth * -0.5f, areaWidth * 0.5f);
            float y = Random.Range(areaHeight * -0.5f, areaHeight * 0.5f);
            return new Vector2(x, y);
        }

        public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
        {
            if(propertiesThatChanged.ContainsKey(TileIdPropertiesKey))
            {
                RemoveAllTiles();

                int[] tileIds = (int[])propertiesThatChanged[TileIdPropertiesKey];
                Vector2[] tilePositions = (Vector2[])propertiesThatChanged[TilePositionPropertiesKey];
                bool[] isOddData = (bool[])propertiesThatChanged[TileIsOddPropertiesKey];


                StartCoroutine(SpawnRoomTiles(tileIds, tilePositions, isOddData, this.spawnDelay));

                roundEnded = false;
            }
        }

        IEnumerator SpawnRoomTiles(int[] tileIds, Vector2[] tilePositions, bool[] isOddData, float spawnDelay)
        {
            for(int i = 0; i < tileIds.Length; i++)
            {
                GenerateNewTile(tiles[tileIds[i]], tilePositions[i], isOddData[i], i);
                yield return new WaitForSeconds(spawnDelay);
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(Vector3.zero, new Vector3(spawnAreaWidth, spawnAreaHeight, 0));
        }
    }
}
