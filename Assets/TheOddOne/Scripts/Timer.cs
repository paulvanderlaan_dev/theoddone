﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    [SerializeField] private TMP_Text timerText;

    private float currentTime;
    private bool isRunning;

    private void Update()
    {
        if(isRunning)
        {
            currentTime += Time.deltaTime;
            timerText.text = currentTime.ToString("n2");
        }
    }

    public void StartTimer()
    {
        isRunning = true;
        timerText.text = currentTime.ToString("n2");
    }
    public void ResetTime()
    {
        currentTime = 0f;
        timerText.text = currentTime.ToString("n2");
    }

    public float ClockTime()
    {
        isRunning = false;
        return currentTime;
    }
}
